#  COLLECTIONS IN JAVA

  We use data structures like arrays for storing and manipulating different objects. We would be having a fixed size for the array and we programmers will fix the size in the coding
  phase. But what if we want our data structures size to be changed according to the input we give at the run time? Here comes the role of java collection framework!
   The java collection framework contains number of classes or architectures which will provide the features like storing and manipulating different objects dynamically.

 
 
  Here are some basic operations which is possible in any collection
  
  - Adding objects dynamically
  
  - Removing objects from collection
  
  - Iterating through collection and listing objects
  
  - Retrieving objects
  
  - Searching objects 
  
  


  **JAVA COLLECTION HIERARCHY** 


![alt](java-collection-hierarchy.png)


  **ITERABLE** 

  The framework contains an interface iterable. Iterable interface provides the iterator to iterate through all the collections.

  **THE COLLECTION INTERFACES**
  
  An interface can have methods and variables. The methods defined in the interface are abstract by default.

  **LIST**

  In list we will be storing data sequentially. List is very helpful in the cases where we want the objects in the same order we have given. The list doesn't care about the data 
  duplication. Different types of lists are arraylist, Linked list, Vector and stack. Linked list will have two values in each entry, One is the data and the other is the reference to the next object. Vectors are dynamically resizeable arrays. We can access values from anywhere in the vector like arrays. Stack works in a last in first out (LIFO) manner.
  
  List example :

  ```
  import java.util.*;  
  public class ListExample1
  {  
      public static void main(String args[]){  
      List<String> list=new ArrayList<String>();    
      list.add("Mango");  
      list.add("Apple");  
      list.add("Banana");  
      list.add("Grapes");   
      for(String fruit:list)  
        System.out.println(fruit);       
    }  
    }  
  ```

  **SET**

  Set interface doesn't allow data duplication. We will have a set of non duplicated objects in the collection. Here set wouldn't be keeping track of the order of the objects given. There are  Three set of implementations in this interface which are, Hashset, Tree set and linked list hashset. Hash set enables you to store data in a hash table, And it will not care about order in which the data will appear. Tree set allows to store ellements in red black order, It is slower than hash set. In linked list hash set the elements are stored in linked list order in a hash table, This will guarantee the order of element insertion or deletion.

  Set example : 

  ```
    import java.util.*;   
    public class setExample{   
    public static void main(String[] args)   
    {   
        Set<String> data = new LinkedHashSet<String>();   
        data.add("Java");   
        data.add("Set");   
        data.add("Example");   
        data.add("Set");   
        System.out.println(data);   
    }   
   } 
```

  **QUEUE**

  Queue follows a first in first out (FIFO) method, That is we will have data in the order of their arrival. There is another type of queue which is dequeue. Dequeue will allow both first in first out (FIFO) and last in first out (LIFO). There is also priority queue in which the data can be retrieved in the order of priority (eg: highest value number or lowest value number) and dynamically rearrange the size of the collection.

Queue example : 

```
import java.util.*;  
class TestCollection12{  
public static void main(String args[]){  
  PriorityQueue<String> queue=new PriorityQueue<String>();  
  queue.add("swabeeh");  
  queue.add("Vijay");  
  queue.add("Karan");  
  System.out.println("head:"+queue.element());  
  System.out.println("head:"+queue.peek());  
  System.out.println("iterating the queue elements:");  
  Iterator itr=queue.iterator();  
  while(itr.hasNext()){  
    System.out.println(itr.next());  
    }  
  queue.remove();  
  queue.poll();  
  System.out.println("after removing two elements:");  
  Iterator<String> itr2=queue.iterator();  
  while(itr2.hasNext()){  
    System.out.println(itr2.next());  
    }  
  }  
} 
```


  References :
   
  * https://www.javatpoint.com/collections-in-java
  * https://www.geeksforgeeks.org/